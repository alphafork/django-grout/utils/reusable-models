# reusable-models

Utility script to fetch model strings from application's AppConfig


## Usage

### Save model strings to AppConfig

Save a `model_strings` dictionary inside app's `AppConfig` of the apps.

eg.

``` python
from django.apps import AppConfig

class ImsStudentConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ims_student"

    model_strings = {
        'ACADEMIC':'Academic',
    }
```

### Selecting app list names

You can select apps from custom app lists, other than `INSTALLED_APPS`. To add a custom list, use `APP_LIST_NAMES` variable in project settings. If you want to add multiple lists, use an array. Otherwise give the list's name as string. 

Script searches `INSTALLED_APPS` by default, to check none,

`APP_LIST_NAMES = ""`

### Accessing model class

``` python
from reusable_models import get_model_from_string

model_class = get_model_from_string('MODEL_STRING')
```

### Notes

- In case of multiple instances of a string is found in different apps, string from the last app is used.


## License

[AGPL-3.0-or-later](LICENSE)


## Contact

Alpha Fork Technologies

[connect@alphafork.com](mailto:connect@alphafork.com)
