from django.conf import settings
from django.apps import apps


def get_applications_path():
    applications_path = ""
    try:
        applications_path = settings.REUSABLE_MODELS_LOOKUP_APPS_LISTS
    except:
        applications_path = "INSTALLED_APPS"
    return applications_path


def get_model_from_string(model_string):
    return get_model(model_string)


def get_model(model_string):
    application_model = ""
    applications_path = get_applications_path()

    if type(applications_path) == list:
        for application_path in applications_path:
            application_model = string_to_model(application_path, model_string)
    elif type(applications_path) == str:
        application_model = string_to_model(applications_path, model_string)
    if application_model:
        return application_model
    raise LookupError(model_string + " lookup failed.")


def string_to_model(application_path, model_string):
    applications = getattr(settings, application_path)
    application_model = ""

    for application in applications:
        application_module = apps.get_app_config(application)
        try:
            application_model = getattr(
                application_module.models_module,
                application_module.model_strings[model_string],
            )
            return application_model
        except:
            pass

    return None
